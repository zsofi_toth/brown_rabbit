$( "#nav-links-icon" ).click(function() {
    //TODO: toggle navigation
});

//Simple Slider
var currentIndex = 0,
  items = $('.c-header__slider-container img'),
  itemAmt = items.length;

function cycleItems() {
  var item = $('.c-header__slider-container img').eq(currentIndex);
  items.hide();
  item.css('display','inline-block');
}

var autoSlide = setInterval(function() {
  currentIndex += 1;
  if (currentIndex > itemAmt - 1) {
    currentIndex = 0;
  }
  cycleItems();
}, 3000);

